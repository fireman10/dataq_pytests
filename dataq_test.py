import configparser
import pytest
import requests
import template
from datetime import datetime
import json
import time
from dq import DQYaml



# reading config.ini file
cfg = configparser.ConfigParser()
cfg.read('pytest.ini')


@pytest.fixture
def session():
    """ Returns DMS session ID
        :return: str dms session id
        """
    payload = f"action=login&username={cfg.get('user', 'user_name')}&password={cfg.get('user', 'password')}"
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    try:
        response = requests.post(f"{cfg.get('app', 'base_url')}/myapp", headers=headers, data=payload)
        yield response.json().get("session_id")
        print(f"Calling Fixtures")
    except Exception:
        raise Exception("Unable to create session session id")

def create_project(session):
    """ Create DMS Project ID
        :return: Null
        """
    body = {"name": cfg.get('app', 'project_name'),
            "description": "test",
            "iconId": "0",
            "groupName": cfg.get('app', 'group_name'),
            "session_id": session,
            "session.id": session}

    headers = {"sessionid": session}
    try:
        response = requests.post(f"{cfg.get('app', 'base_url')}/dms/projects", headers=headers, json=body)
        print(f"body: {body}")
        print(f"response: {response.json()}")
    except Exception:
        raise Exception("Error while creating project")


@pytest.fixture
def project_id(session):
    """ Returns DMS Project ID
    :return: str dms project id
    """
    create_project(session)
    url_parms = {
        "groupName": cfg.get('app', 'group_name'),
    }
    headers = {"sessionid": session}
    params_str = "&".join([f"{k}={v}" for k, v in url_parms.items() if v])
    try:
        response = requests.get(f"{cfg.get('app', 'base_url')}/dms/projects?{params_str}", headers=headers)
        yield [data['id'] for data in response.json() if data['name'] == cfg.get('app', 'project_name')][0]
    except Exception:
        raise Exception("Error was raised while getting DMS project ID")


def execute_flow(flow_id_list: list, session, project_id, successful_emails: str = None, failure_emails: str = None):
    """ Triggers dq flow
    :param project_id:str
    :param successful_emails:optional[str]
    :param failure_emails:optional[str]
    :param session:required[str]
    :return: dict
    """
    payload_params = {
        "parallelJobExecutions": len(flow_id_list),
        "dbConnections": 1,
        "flowExecutionConfigs": [{
            "azkabanProjectId": project_id,
            "successEmails": successful_emails,
            "failureEmails": failure_emails,
            "successEmailsOverride": 'true',
            "failureEmailsOverride": 'true',
            "groupId": cfg.get('app', 'group_name'),
            "executors": 1,
            "executorCores": 3,
            "parameters": []
        } for project_id in flow_id_list
        ]
    }
    headers = {"sessionid": session,
               "session_id": session,
               "Content-Type": 'application/json'
               }
    try:
        response = requests.post(f"{cfg.get('app', 'base_url')}/dms/projects/{project_id}/batches/execute?",
                                 json=payload_params, headers=headers)
        return response
    except Exception:
        raise Exception("Error was raised while triggering flow")


def execution_result(execution_id: str, session):
    headers = {
        "sessionid": session,
    }
    try:
        return requests.get(f"{cfg.get('app', 'base_url')}/dms/executions/{execution_id}", headers=headers).json()

    except Exception:
        raise Exception("Error was raised while flow result execution")


def get_execution_details(session, execution_id):
    payload = {'session.id': session,
               'session_id': session,
               'ajax': 'fetchDVResult',
               'execid': execution_id,
               'action': 'fetch'
               }
    try:
        session = requests.Session()
        print(f"GET SESSION ID :Session : {session}")
        response = session.post(f"{cfg.get('app', 'base_url')}/myapp/dvResults?", data=payload)
        print(response.json())
        return response.json()
    except Exception:
        raise Exception("Error was raised while flow result execution")


def get_execution_result(execution_id: str, session, timeout: int = 600, sleep: int = 15):
    end_time = time.time() + timeout
    attempt = 1
    result = execution_result(execution_id, session)
    while time.time() < end_time and result['batchExecutionSummary']['batchTestStatus'] == 'IN_PROGRESS':

        result = execution_result(execution_id, session)
        # print(f"Results = {result}")
        if result['batchExecutionSummary']['batchTestStatus'] == 'IN_PROGRESS':
            attempt += 1
            time.sleep(sleep)
    return result


def create_execute_get_execution_details(payload, flow_name, session, project_id, expected_result):
    url_parm = {
        "dmsProjectId": project_id,
        "project_name": f"dq{datetime.now().strftime('%Y%m%d%H%M%S%f')}",
        "project_description": flow_name,
        "session_id": session,
        "session.id": session,

    }
    params = json.dumps(payload)
    # print(params)

    params_str = "&".join([f"{k}={v}" for k, v in url_parm.items() if v])

    try:
        flow_create_response = requests.post(f"{cfg.get('app', 'base_url')}/myapp/dvFlowUpload?{params_str}",
                                             data=params)
        # print("Flow created successfully")

    except Exception:
        raise Exception("Error was raised while creating new flow")

    assert flow_create_response.status_code == 200

    # print(f"Flow json:{flow_create_response.json()}")
    flow_id = [flow_create_response.json()['project_id']]
    # print(f"Flow ID: {flow_id}")
    # executing flow
    execution_response = execute_flow(flow_id, session, project_id)
    assert execution_response.status_code == 200

    # print(f"execution_response: {execution_response.json()}")
    batch_execution_id = execution_response.json()['batchExecutionId']
    # print(f"batch_execution_id: {batch_execution_id}")

    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result

    return get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])


@pytest.mark.datamigration
def test_data_migration1(session, project_id):  # ALL DATA MATCHING
    flow_name = 'test_datamigration1'
    payload = template.data_migration_template
    payload['jobMap']['component0']['sourceData'] = [
        {
            "datasetFormat": "JDBC",
            "databaseName": "tims_db",
            "datasetPath": "projects",
            "jdbcData": {
                "connectionName": "tims_db"
            },
            "tableNamesList": {
                "Person_Detail": None
            },
            "tableProperties": [
                {
                    "tblName": "Person_Detail",
                    "tblSize": "1",
                    "tblRecordCount": 13
                }
            ]
        }
    ]
    payload['jobMap']['component1']['sourceData'] = [
        {
            "datasetFormat": "JDBC",
            "databaseName": "tims_db",
            "datasetPath": "projects",
            "jdbcData": {
                "connectionName": "tims_db"
            },
            "tableNamesList": {
                "Person_Detail": None
            },
            "tableProperties": [
                {
                    "tblName": "Person_Detail",
                    "tblSize": "1",
                    "tblRecordCount": 13
                }
            ]
        }
    ]
    payload['jobMap']['component2']['filesCompareList'] = [
        {
            "compareType": "CELL_BY_CELL_COMPARE",
            "compareColumns": False,
            "compareCommonColumnsOnly": True,
            "srcDestTableMap": {
                "Person_Detail": "Person_Detail"
            },
            "srcDestComponentMap": {
                "component0": "component1"
            },
            "saveAllDiffOutput": "",
            "validateRowsCount": False,
            "tolerance": [
            ],
            "sqlTransform": [
            ],
            "columnMapping": [
                {
                    "srcColumn": "Person_Age",
                    "destColumn": "Person_Age"
                },
                {
                    "srcColumn": "Person_ID",
                    "destColumn": "Person_ID"
                },
                {
                    "srcColumn": "Person_Name",
                    "destColumn": "Person_Name"
                },
                {
                    "srcColumn": "Person_Salary",
                    "destColumn": "Person_Salary"
                },
                {
                    "srcColumn": "Person_SalaryCode",
                    "destColumn": "Person_SalaryCode"
                },
                {
                    "srcColumn": "Person_ZipCode",
                    "destColumn": "Person_ZipCode"
                }
            ],
            "matchBoth": True,
            "keyValueProperties": {
                "DATA_MIGRATION": "True"
            }
        }
    ]

    expected_result = 'PASSED'
    execution_details = create_execute_get_execution_details(payload, flow_name, session, project_id, expected_result)
    srcrecordcount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['srcRecordsCount']
    destrecordcount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['destRecordsCount']
    srckeycount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['srcKeyCount']
    destkeycount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['destKeyCount']
    sbrodcast = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['srcKeyMatchBrodcast']
    nmatching = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['totalDestRecordsNotMatching']
    dbrodcast = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['destKeyMatchBrodcast']
    srcccount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['srcColumnsCount']
    tmatching = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['totalSrcRecordsMatching']
    tdmatching = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['totalDestRecordsMatching']
    tnmatching = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['totalSrcRecordsNotMatching']
    dccount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['destColumnsCount']
    test_data_migration1_output = {
    "srcRecordsCount": 12,
    "destRecordsCount": 12,
    "srcKeyCount": 0,
    "destKeyCount": 0,
    "srcKeyMatchBrodcast": 0,
    "totalDestRecordsNotMatching": 0,
    "destKeyMatchBrodcast": 0,
    "srcColumnsCount": 6,
    "totalSrcRecordsMatching": 12,
    "totalDestRecordsMatching": 12,
    "totalSrcRecordsNotMatching": 0,
    "destColumnsCount": 6
    }

    assert srcrecordcount == test_data_migration1_output['srcRecordsCount']
    assert destrecordcount == test_data_migration1_output['destRecordsCount']
    assert srckeycount == test_data_migration1_output['srcKeyCount']
    assert destkeycount == test_data_migration1_output['destKeyCount']
    assert sbrodcast == test_data_migration1_output['srcKeyMatchBrodcast']
    assert nmatching == test_data_migration1_output['totalDestRecordsNotMatching']
    assert dbrodcast == test_data_migration1_output['destKeyMatchBrodcast']
    assert srcccount == test_data_migration1_output['srcColumnsCount']
    assert tmatching == test_data_migration1_output['totalSrcRecordsMatching']
    assert tdmatching == test_data_migration1_output['totalDestRecordsMatching']
    assert tnmatching == test_data_migration1_output['totalSrcRecordsNotMatching']
    assert dccount == test_data_migration1_output['destColumnsCount']

@pytest.mark.datamigration
def data_migration_all(session, project_id):  # ALL DATA MATCHING
    flow_name = 'test_datamigration2'
    payload = template.data_migration_template3

    expected_result = 'PASSED'
    execution_details = create_execute_get_execution_details(payload, flow_name, session, project_id, expected_result)
    srcrecordcount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['srcRecordsCount']
    destrecordcount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['destRecordsCount']
    srckeycount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['srcKeyCount']
    destkeycount = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']['destKeyCount']


def data_migration2(session, project_id):
    flow_desc = 'test_migration_tablecount'
    payload = template.data_migration_template2
    expected_result = 'PASSED'
    execution_details = create_execute_get_execution_details(payload, flow_desc, session, project_id, expected_result)
    print("execution_details")
    print(execution_details)


