data_migration_template = {
    "executorCores":2,
    "executorMemory":"2g",
    "email":[
    ],
    "flowName":"dataq1635141949940",
    "flowDesc":"test1",
    "hadoop":False,
    "jobMap":{
        "component0":{
            "dependentJob":True,
            "isLeft": True,
            "type": "InputSource",
            "description":"Source",
            "position":{
                "x":181,
                "y":115
            },
            "id":"component0",
            "sourceData":[]
        },
        "component1":{
            "dependentJob":True,
            "type": "InputSource",
            "description":"Target",
            "position":{
                "x":232,
                "y":308
            },
            "id":"component1",
            "sourceData":[]
        },
        "component2":{
            "dependentJob":True,
            "type": "DataCompare",
            "dependsOn":[
                "component0",
                "component1"
            ],
            "jobType":"CELL_BY_CELL_COMPARE",
            "filesCompareList":[],
            "description":"any name",
            "position":{
                "x":457,
                "y":186
            },
            "id":"component2"
        }
    },
    "livy":False,
    "numExecutors":2,
    "root":"component2",
    "flowType":"DATA MIGRATION",
    "executeOn":"LOCAL",
    "API_APP_BUILD":"Oct 14, 2021 10:04:40 PM",
    "UI_VERSION":"144-4737d64095",
    "APP_VERSION":"603-8e6fbcaef8",
    "LicensedName":"microsoft"
    }