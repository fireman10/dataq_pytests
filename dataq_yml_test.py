from requests import sessions
from requests.api import request
from requests.models import Request
from requests.sessions import Session
import dataq_test
import requests
from dataq_test import session
from dq import DQYaml
import pytest
import configparser
from dataq_test import get_execution_details
from dataq_test import get_execution_result

cfg = configparser.ConfigParser()
cfg.read('pytest.ini')


username = cfg.get('user', 'user_name')
password = cfg.get('user', 'password')
url = cfg.get('app', 'base_url')

@pytest.fixture
def session():
    """ Returns DMS session ID
        :return: str dms session id
        """
    payload = f"action=login&username={cfg.get('user', 'user_name')}&password={cfg.get('user', 'password')}"
    headers = {"Content-Type": "application/x-www-form-urlencoded"}
    try:
        response = requests.post(f"{cfg.get('app', 'base_url')}/myapp", headers=headers, data=payload)
        yield response.json().get("session_id")
    except Exception:
        raise Exception("Unable to create session session id")


def match_results(execution_details, output_json):
    executionstats = execution_details['res']['component2']['res']['fileCompareResult'][0]['stats']
    assert executionstats['srcRecordsCount'] == output_json['srcRecordsCount']
    assert executionstats['destRecordsCount'] == output_json['destRecordsCount']
    assert executionstats['srcKeyCount'] == output_json['srcKeyCount']
    assert executionstats['destKeyCount'] == output_json['destKeyCount']
    assert executionstats['srcKeyMatchBrodcast'] == output_json['srcKeyMatchBrodcast']
    assert executionstats['totalDestRecordsNotMatching'] == output_json['totalDestRecordsNotMatching']
    assert executionstats['destKeyMatchBrodcast'] == output_json['destKeyMatchBrodcast']
    assert executionstats['srcColumnsCount'] == output_json['srcColumnsCount']
    assert executionstats['totalSrcRecordsMatching'] == output_json['totalSrcRecordsMatching']
    assert executionstats['totalDestRecordsMatching'] == output_json['totalDestRecordsMatching']
    assert executionstats['totalSrcRecordsNotMatching'] == output_json['totalSrcRecordsNotMatching']
    assert executionstats['destColumnsCount'] == output_json['destColumnsCount']
    assert executionstats['totalCellCount'] == output_json['totalCellCount']
    assert executionstats['matchCellCount'] == output_json['matchCellCount']
    return True

@pytest.mark.datamigration
def test_datamigration(session):
    dqyaml = DQYaml(file="./TC001_dm_cellbycell.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    test_data_migration2_output = {
    "srcKeyMatchBrodcast": 32,
    "destKeyCount": 32,
    "totalCellCount": 192,
    "totalDestRecordsNotMatching": 0,
    "srcSize": 100,
    "destSize": 100,
    "srcRecordsCount": 32,
    "columnsNotMatchingMap": {},
    "srcKeyCount": 32,
    "destRecordsCount": 32,
    "columnsNotMatching": {},
    "destKeyNoMatch": 0,
    "destKeyMatchBrodcast": 32,
    "srcKeyNoMatch": 0,
    "destKeyMatchPartial": 0,
    "srcColumnsCount": 3,
    "totalSrcRecordsMatching": 32,
    "srcKeyMatchPartial": 0,
    "totalDestRecordsMatching": 32,
    "totalSrcRecordsNotMatching": 0,
    "destColumnsCount": 3,
    "matchCellCount": 192
}
    expected_result = "PASSED"
    results = dqyaml.get_result_yaml(extended= True)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])
    #assert match_results(execution_details, test_data_migration2_output)


@pytest.mark.datamigration
def test_datamigration2(session):

    dqyaml = DQYaml(file="./TC002_dm_cellbycell2.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result


@pytest.mark.datamigration
def test_datamigration3(session):

    dqyaml = DQYaml(file="./TC003_dm_cellbycell3.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result

@pytest.mark.etl
def test_datamigration4(session):
    dqyaml = DQYaml(file="./TC004_etl_cellbycell.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result

@pytest.mark.etl
def test_datamigration5(session):
    dqyaml = DQYaml(file="./TC005_etl_cellbycell.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result

@pytest.mark.etl
def test_datamigration6(session):
    # checking duplicate records
    dqyaml = DQYaml(file="./TC006_etl_cellbycell.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result

@pytest.mark.profile
def test_datamigration7(session):
    # checking
    dqyaml = DQYaml(file="./TC008_dp.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "PASSED"
    results = dqyaml.get_result_yaml(extended= True)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result

@pytest.mark.etlbigdata
def test_datamigration8(session):
    test_data_migration9_output = {
        "srcKeyMatchBrodcast": 2097152,
        "destKeyCount": 2097152,
        "totalCellCount": 140000000,
        "totalDestRecordsNotMatching": 0,
        "srcSize": 100,
        "destSize": 100,
        "srcRecordsCount": 2000004,
        "columnsNotMatchingMap": {},
        "srcKeyCount": 2097152,
        "destRecordsCount": 5000000,
        "columnsNotMatching": {},
        "destKeyNoMatch": 0,
        "destKeyMatchBrodcast": 2097152,
        "srcKeyNoMatch": 0,
        "destKeyMatchPartial": 0,
        "srcColumnsCount": 14,
        "totalSrcRecordsMatching": 5000000,
        "srcKeyMatchPartial": 0,
        "totalDestRecordsMatching": 5000000,
        "totalSrcRecordsNotMatching": 0,
        "destColumnsCount": 14,
        "matchCellCount": 140000000
    }
    dqyaml = DQYaml(file="./TC009_etl_cellbycell.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 2200)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])

@pytest.mark.etlbigdataonhadoop
def test_datamigration9(session):
    test_data_migration9_output = {
        "srcKeyMatchBrodcast": 2097152,
        "destKeyCount": 2097152,
        "totalCellCount": 140000000,
        "totalDestRecordsNotMatching": 0,
        "srcSize": 100,
        "destSize": 100,
        "srcRecordsCount": 5000000,
        "columnsNotMatchingMap": {},
        "srcKeyCount": 2097152,
        "destRecordsCount": 5000000,
        "columnsNotMatching": {},
        "destKeyNoMatch": 0,
        "destKeyMatchBrodcast": 2097152,
        "srcKeyNoMatch": 0,
        "destKeyMatchPartial": 0,
        "srcColumnsCount": 14,
        "totalSrcRecordsMatching": 5000000,
        "srcKeyMatchPartial": 0,
        "totalDestRecordsMatching": 5000000,
        "totalSrcRecordsNotMatching": 0,
        "destColumnsCount": 14,
        "matchCellCount": 140000000
    }
    dqyaml = DQYaml(file="./TC0010_etl_cellbycell.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 3600)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])


@pytest.mark.datamigration
def test_datamigration11(session):
    dqyaml = DQYaml(file="./TC0011_DataMigrationTesting.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 1200)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])


@pytest.mark.datamigration
def test_datamigration12(session):
    # Auto map With Exclude Feature Testing
    dqyaml = DQYaml(file="./TC0012_AutoMapping_excludeTesting.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 1200)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])


@pytest.mark.schemacompare
def test_datamigration13(session):
    # Auto map With Exclude Feature Testing
    dqyaml = DQYaml(file="./TC0013_SchemaCompareTesting.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 1200)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])
    
@pytest.mark.schemacompare
def test_datamigration14(session):
    # Auto map With Exclude Feature Testing
    dqyaml = DQYaml(file="./TC014_Yaml_ETLTesting.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 1200)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    #execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])
    
@pytest.mark.dataquality
def test_datamigration15(session):
    # Data Quality with Data Profile True
    dqyaml = DQYaml(file="./TC0015_DataQuality_dataprofile_true.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 1200)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    #execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])

@pytest.mark.dataquality
def test_datamigration16(session):
    # Data Quality with Data Profile True
    dqyaml = DQYaml(file="./TC0016_DataQuality_observations.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 3600)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    #execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])


@pytest.mark.dataquality
def test_datamigration17(session):
    # Data Quality with Data Profile True
    dqyaml = DQYaml(file="./TC0017_DataQuality_person.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "PASSED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 1200)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    #execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])


@pytest.mark.dataquality
def test_datamigration18(session):
    # Data Quality with Data Profile True
    dqyaml = DQYaml(file="./TC0018_ETL_grouping.yml", url= url, username= username, password= password)
    jobs = dqyaml.dq.jobs
    expected_result = "FAILED"
    results = dqyaml.get_result_yaml(extended= True, timeout= 1200)
    batch_execution_id = results[0]['batchExecutionSummary']['id']
    get_execution_result_value = get_execution_result(str(batch_execution_id), session)
    assert get_execution_result_value['batchExecutionSummary']['batchTestStatus'] == expected_result
    #execution_details = get_execution_details(session, get_execution_result_value['batchExecutionFlows'][0]['executionId'])
